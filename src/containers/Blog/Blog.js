import React, { Component } from "react";
import axios from "axios";
import Post from "../../components/Post/Post";
import FullPost from "../../components/FullPost/FullPost";
import NewPost from "../../components/NewPost/NewPost";
import "./Blog.css";

class Blog extends Component {
	state = {
		posts: [],
		selectedId: null,
		err: false
	};
	componentDidMount() {
		axios
			.get("/posts")
			.then(response => {
				const posts = response.data.splice(0, 4);
				const updatedPosts = posts.map(post => {
					return {
						...post,
						author: "Abhiyan"
					};
				});
				this.setState({
					posts: updatedPosts
				});
			})
			.catch(err => {
				this.setState({
					err: true
				});
			});
	}
	clickedPostHandler = id => {
		this.setState({
			selectedId: id
		});
	};
	render() {
		let posts;
		if (!this.state.err) {
			posts = this.state.posts.map(post => {
				return (
					<Post
						key={post.id}
						title={post.title}
						author={post.author}
						clicked={() => this.clickedPostHandler(post.id)}
					/>
				);
			});
		} else {
			posts = <p style={{ textAlign: "center" }}>Something went wrong</p>;
		}
		return (
			<div>
				<section className="Posts">{posts}</section>
				<section>
					<FullPost id={this.state.selectedId} />
				</section>
				<section>
					<NewPost />
				</section>
			</div>
		);
	}
}

export default Blog;
